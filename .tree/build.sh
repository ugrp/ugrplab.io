#!/bin/bash

# Current working directory, folder where script was called from
CMD_DIR=${1:-"$PWD"}

echo "$CMD_DIR"

# parameters expansion, if provided use those, else fallback to environment variables or defaults
ARTIFACTS_PATH=${2:-${ARTIFACTS_PATH:-"public"}}
TREE_URL=${3:-${TREE_URL:-"https://ugrp.gitlab.io"}}
TREE_TITLE=${4:-${TREE_TITLE:-"¿Ça va!"}}

# prepare build folder for publishing
rm -rf .public public
mkdir .public
cp -r * .public
cp -r .{tree,info,gitignore} .public
mv .public public
cd public

# HTML output of tree results for all files
tree_output=$(tree \
    -a \
    -L 2 \
    --gitfile="${CMD_DIR}/.tree/.treeignore" \
    --hintro="${CMD_DIR}/.tree/hintro.html" \
    --houtro="${CMD_DIR}/.tree/houtro.html" \
    --noreport\
    -H "$TREE_URL" \
    -T "$TREE_TITLE" \
    -C \
    --infofile="${CMD_DIR}/.info" \
    --info \
    -I "__*" \
    "${CMD_DIR}")


echo "$tree_output" > "${CMD_DIR}/${ARTIFACTS_PATH}/index.html"
