# <username.gitlab.io>
Welcome! This is a txt file so it can be read in the browser.

This site is mainly HTML redirects to other pages.

# Develop
To develop locally
```bash
.tree/build.sh && npx serve public
# or (not used in current state)
./.tree/build.sh "https://myurl.gitlab.io" "My Webpage Title"
```

# txt
is the format of this doc, so browser offer to read it,
instead of downloading it such as for markdown files
(no markdown renderer in most browsers).
