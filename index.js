(({
	message = "Welcome!",
	char = String.fromCharCode(Math.floor(Math.random() * 65535)),
	color = `#${(0x1000000 + Math.random() * 0xffffff)
		.toString(16)
		.substr(1, 6)}`,
	hashMap = new URLSearchParams(window.location.hash.slice(1)),
	queryMap = new URLSearchParams(window.location.search),
	iframe = document.querySelector("iframe"),
} = {}) => {
	console.info(message, hashMap, queryMap);
	document.title = char;
	document.documentElement.style.setProperty("--color-random", color);
	document.documentElement.style.setProperty("--title", char);
	iframe.setAttribute("src", iframe.src + window.location.hash);
})();
